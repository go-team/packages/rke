Source: rke
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Arthur Diniz <arthurbdiniz@gmail.com>
Build-Depends: debhelper (>= 12),
               dh-golang,
               golang-github-blang-semver-dev,
               golang-github-go-ini-ini-dev,
               golang-github-mattn-go-colorable-dev,
               golang-github-mcuadros-go-version-dev,
               golang-github-pkg-errors-dev,
               golang-github-sirupsen-logrus-dev,
               golang-github-urfave-cli-dev,
               golang-gopkg-yaml.v2-dev
Build-Depends-Arch: golang-any
Standards-Version: 4.4.1
Homepage: https://github.com/rancher/rke
Vcs-Browser: https://salsa.debian.org/go-team/packages/rke
Vcs-Git: https://salsa.debian.org/go-team/packages/rke.git
XS-Go-Import-Path: github.com/rancher/rke
Testsuite: autopkgtest-pkg-go

Package: rke
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Extremely simple, lightning fast Kubernetes installer that works everywhere
 Rancher Kubernetes Engine (RKE) is a light-weight Kubernetes installer that
 supports installation on bare-metal and virtualized servers.
 .
 RKE solves a common issue in the Kubernetes community: installation complexity.
 With RKE, Kubernetes installation is simplified, regardless of what operating
 systems and platforms you’re running.
