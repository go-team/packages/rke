#!/bin/bash

help2man rke --no-discard-stderr --version-string="$(dpkg-parsechangelog -S Version | cut -d- -f1)" -n "Rancher Kubernetes Engine, an extremely simple, lightning fast Kubernetes installer that works everywhere" > debian/rke.1
